# LeBonCoin

## Test technique LeBonCoin 

Réalisation d'une application Android native en Kotlin suivant une architecture MVVM.

## Les principales libraires  

### Room :
Permet de sauvegarder des données en local dans l'app-database

### Retrofit :
Permet de récupérer des informations via des API HTTP

### Livedata :
Permet d'observer de façon cycle-aware des datas

### Coroutines :
Permet de gérer le code de façon asynchrone 

### Glide :
Permet d'afficher des images

### Mockito : 
Permet de faciliter la rédaction des tests unitaires 

### Espresso :
 Permet de faire des tests fonctionnels
