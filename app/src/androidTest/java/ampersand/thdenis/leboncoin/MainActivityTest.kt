package ampersand.thdenis.leboncoin


import android.view.View
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun mainActivityTest() {

        onView(withId(R.id.rv_musics))
            .check(
                matches(
                    atPosition(
                        0,
                        withChild(
                            withChild(
                                allOf(
                                    withId(R.id.tv_title),
                                    withText("accusamus beatae ad facilis cum similique qui sunt"),
                                    isDisplayed()
                                )
                            )
                        )
                    )
                )
            )
        onView(withId(R.id.rv_musics))
            .check(
                matches(
                    atPosition(
                        0,
                        withChild(withChild(allOf(withId(R.id.thumbnail), isDisplayed())))
                    )
                )
            )

        onView(withId(R.id.rv_musics)).perform(actionOnItemAtPosition<ViewHolder>(0, click()))


        // Enter inside music detail fragment
        Thread.sleep(2000)

        onView(
            allOf(
                withId(R.id.iv_url),
                withContentDescription("Image du titre actuel"),
                isDisplayed()
            )
        ).check(matches(isDisplayed()))

        onView(
            allOf(
                withId(R.id.tv_current_title),
                withText("accusamus beatae ad facilis cum similique qui sunt"),
                isDisplayed()
            )
        ).check(matches(isDisplayed()))

        onView(
            allOf(
                withId(R.id.tv_other_musics),
                isDisplayed()
            )
        ).check(matches(withText("Les autres musiques du même album :")))


        onView(withId(R.id.rv_musics))
            .check(
                matches(
                    atPosition(
                        0,
                        withChild(
                            withChild(
                                allOf(
                                    withId(R.id.tv_title),
                                    withText("accusamus beatae ad facilis cum similique qui sunt"),
                                    isDisplayed()
                                )
                            )
                        )
                    )
                )
            )
        onView(withId(R.id.rv_musics))
            .check(
                matches(
                    atPosition(
                        0,
                        withChild(withChild(allOf(withId(R.id.thumbnail), isDisplayed())))
                    )
                )
            )
        onView(withId(R.id.rv_musics)).perform(actionOnItemAtPosition<ViewHolder>(0, click()))
    }

    private fun atPosition(
        position: Int,
        @NonNull itemMatcher: Matcher<View?>
    ): Matcher<View?>? {
        checkNotNull(itemMatcher)
        return object : BoundedMatcher<View?, RecyclerView>(RecyclerView::class.java) {
            override fun describeTo(description: Description) {
                description.appendText("has item at position $position: ")
                itemMatcher.describeTo(description)
            }

            override fun matchesSafely(view: RecyclerView): Boolean {
                val viewHolder =
                    view.findViewHolderForAdapterPosition(position)
                        ?: // has no item on such position
                        return false
                return itemMatcher.matches(viewHolder.itemView)
            }
        }
    }
}
