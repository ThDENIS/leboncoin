package ampersand.thdenis.leboncoin

import ampersand.thdenis.leboncoin.dao.AppDatabase
import ampersand.thdenis.leboncoin.dao.MusicDao
import ampersand.thdenis.leboncoin.helper.HelperTest
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import junit.framework.Assert.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.util.concurrent.CountDownLatch


@RunWith(RobolectricTestRunner::class)
@Config(sdk = [28])
class RoomTest : HelperTest() {
    private val context = InstrumentationRegistry.getInstrumentation().context
    private lateinit var dao: MusicDao
    private lateinit var database: AppDatabase
    private var isSuccess: Boolean = false
    private lateinit var latch: CountDownLatch

    @Before
    override fun setUp() {
        super.setUp()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        dao = database.musicDao()
        latch = CountDownLatch(1)
        isSuccess = false
        GlobalScope.launch {
            dao.insertAll(musics)
        }
    }

    @After
    fun tearDown() {
        database.close()
    }

    @Test
    fun test_insertAll_and_getMusic_functional() {
        GlobalScope.launch {
            isSuccess = musics[0] == dao.getMusic(1)
            latch.countDown()
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        assertTrue(isSuccess)
    }

    @Test
    fun test_insertAll_and_getAllMusics_functional() {
        GlobalScope.launch {
            isSuccess = musics == dao.getAllMusics()
            latch.countDown()
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        assertTrue(isSuccess)
    }

    @Test
    fun test_insertAll_and_getAllMusicsFromCurrentAlbum_functional() {
        GlobalScope.launch {
            isSuccess = album1 == dao.getAllMusicsFromCurrentAlbum(album1[0].id)
                    && album2 == dao.getAllMusicsFromCurrentAlbum(album2[0].id)
            latch.countDown()
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        assertTrue(isSuccess)
    }

    @Test
    fun test_insertAll_and_getFirstMusic_functional() {
        GlobalScope.launch {
            isSuccess = musics[0] == dao.getFirstMusic()
            latch.countDown()
        }
        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        assertTrue(isSuccess)
    }

}