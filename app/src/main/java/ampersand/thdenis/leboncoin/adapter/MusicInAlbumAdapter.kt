package ampersand.thdenis.leboncoin.adapter

import ampersand.thdenis.leboncoin.R
import ampersand.thdenis.leboncoin.model.Music
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions


class MusicInAlbumAdapter(private var musics: List<Music>) :
    RecyclerView.Adapter<MusicInAlbumAdapter.MusicHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = MusicHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.music_item,
            parent,
            false
        )
    )


    override fun getItemCount() = musics.size


    override fun onBindViewHolder(holder: MusicHolder, position: Int) =
        holder.bind(musics[position])


    fun setMusics(musics: List<Music>) {
        this.musics = musics
        notifyDataSetChanged()
    }

    class MusicHolder(view: View) : RecyclerView.ViewHolder(view) {
        private var tvTitle: TextView = view.findViewById(R.id.tv_title)
        private var ivThumbnail: ImageView = view.findViewById(R.id.thumbnail)

        fun bind(music: Music) {
            tvTitle.text = music.title
            val url = GlideUrl(
                music.thumbnailUrl, LazyHeaders.Builder()
                    .addHeader("User-Agent", "user-agent")
                    .build()
            )

            Glide.with(itemView.context)
                .load(url)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(ivThumbnail)

            itemView.setOnClickListener {
                val bundle = bundleOf("id" to music.id)
                itemView.findNavController().navigate(R.id.action_DetailMusicFragment_self, bundle)
            }
        }
    }
}