package ampersand.thdenis.leboncoin

import ampersand.thdenis.leboncoin.dao.AppDatabase
import ampersand.thdenis.leboncoin.repository.MusicRepository
import android.app.Application

class MusicApplication : Application() {

    private val database by lazy { AppDatabase.getDatabase(this) }
    val repository by lazy { MusicRepository(database.musicDao()) }
}