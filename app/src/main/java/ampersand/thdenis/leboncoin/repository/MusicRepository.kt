package ampersand.thdenis.leboncoin.repository

import ampersand.thdenis.leboncoin.api.RetrofitInstance
import ampersand.thdenis.leboncoin.dao.MusicDao
import ampersand.thdenis.leboncoin.model.Music
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class MusicRepository(private val musicDao: MusicDao) {

    var allMusics: Flow<List<Music>> = musicDao.getAllMusicsFlow()
    var allMusicsFromAlbum: Flow<List<Music>> = musicDao.getAllMusicsFromCurrentAlbumFlow(1)
    var currentMusic: Flow<Music> = musicDao.getFirstMusicFlow()

    fun getMusics(allMusic: MutableLiveData<List<Music>>) {
        val call: Call<List<Music>> = RetrofitInstance.api.getMusics()
        call.enqueue(object : Callback<List<Music>> {
            override fun onResponse(call: Call<List<Music>>, response: Response<List<Music>>) {
                allMusic.value = response.body()
            }

            override fun onFailure(call: Call<List<Music>>, t: Throwable) {
                println("Error")
            }
        })
    }

    fun getMusic(id: Int): Flow<Music> {
        this.currentMusic = musicDao.getMusicFlow(id)
        return currentMusic
    }

    fun getMusicsFromAlbum(musicId: Int) {
        this.allMusicsFromAlbum = musicDao.getAllMusicsFromCurrentAlbumFlow(musicId)
    }

    fun saveAllMusics(allMusic: MutableLiveData<List<Music>>) =
        musicDao.insertAll(allMusic.value)

}